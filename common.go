package lockbox

import (
	"errors"
	"unsafe"
)

// Longint	-2147483648..2147483647	signed 32-bit
// https://docwiki.embarcadero.com/Libraries/Sydney/en/System.Longint

// DWORD is a 4-byte unsigned integer.
// https://docwiki.embarcadero.com/Libraries/Sydney/en/System.Types.DWORD

type (
	// TKey128 = array [0..15] of Byte;
	TKey128 = [16]byte
	// TKey64  = array [0..7] of Byte;
	TKey64 = [8]byte
)

// Cast converts value of *From type to value of *To type.
func Cast[From, To any](f *From) *To {
	return (*To)(unsafe.Pointer(f))
}

func XorMem(mem1 []byte, mem2 []byte) {
	l := len(mem1)
	if len(mem2) < l {
		l = len(mem2)
	}
	for i := 0; i < l; i++ {
		mem1[i] ^= mem2[i]
	}
}

var (
	ErrInvalidFileFormat = errors.New("invalid file format")
)
