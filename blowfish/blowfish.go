package blowfish

import (
	"bytes"
	cryptorand "crypto/rand"
	"io"
	"os"

	"github.com/solsw/binaryhelper"
	lockbox "gitlab.com/solsw/TurboPower/LockBox"
)

// { Blowfish }

const BFRounds = 16 //{ 16 blowfish rounds }

// TBFContext = packed record
//
//	PBox    : array[0..(BFRounds+1)] of LongInt;
//	SBox    : array[0..3, 0..255] of LongInt;
//
// end;
type TBFContext struct {
	PBox [BFRounds + 2]int32
	SBox [4][256]int32
}

const TBFBlockByteSize = 8

// TBFBlock   = array[0..1] of LongInt;     { BlowFish }
type TBFBlock = [2]int32

// TBFBlockEx = packed record
//
//	Xl : array[0..3] of Byte;
//	Xr : array[0..3] of Byte;
//
// end;
type TBFBlockEx struct {
	Xl [4]byte
	Xr [4]byte
}

func initContext(context *TBFContext) {
	// {initialize PArray}
	_ = binaryhelper.CopyFixed(bf_P, &(context.PBox))
	// {initialize SBox}
	_ = binaryhelper.CopyFixed(bf_S, &(context.SBox))
}

func initEncryptBF(key lockbox.TKey128, context *TBFContext) {
	initContext(context)
	// {update PArray with the key bits}
	j := 0
	for i := 0; i < len(context.PBox); i++ {
		var Data int32 = 0
		for k := 0; k < 4; k++ {
			Data = (Data << 8) | int32(key[j])
			j++
			if j >= len(key) {
				j = 0
			}
			context.PBox[i] = context.PBox[i] ^ Data
		}
	}
	// {encrypt an all-zero string using the Blowfish algorithm and}
	// {replace the elements of the P-array with the output of this process}
	var Block TBFBlock
	i := 0
	for {
		encryptBF(context, &Block, true)
		context.PBox[i] = Block[0]
		context.PBox[i+1] = Block[1]
		i += 2
		if i > BFRounds+1 {
			break
		}
	}
	// {continue the process, replacing the elements of the four S-boxes in}
	// {order, with the output of the continuously changing Blowfish algorithm}
	for j := 0; j < 4; j++ {
		i := 0
		for {
			encryptBF(context, &Block, true)
			context.SBox[j][i] = Block[0]
			context.SBox[j][i+1] = Block[1]
			i += 2
			if i > 255 {
				break
			}
		}
	}
}

func encryptBF(context *TBFContext, block *TBFBlock, encrypt bool) {
	var tmpBlock TBFBlockEx
	_ = binaryhelper.CopyFixed(block, &tmpBlock)
	if encrypt { // encrypting
		block[0] = block[0] ^ context.PBox[0]
		// {16 Rounds to go (8 double rounds to avoid swaps)}
		i := 1
		for {
			// {first half round }
			block[1] = block[1] ^ context.PBox[i] ^ (((context.SBox[0][tmpBlock.Xl[3]] +
				context.SBox[1][tmpBlock.Xl[2]]) ^ context.SBox[2][tmpBlock.Xl[1]]) +
				context.SBox[3][tmpBlock.Xl[0]])
			// {second half round }
			block[0] = block[0] ^ context.PBox[i+1] ^ (((context.SBox[0][tmpBlock.Xr[3]] +
				context.SBox[1][tmpBlock.Xr[2]]) ^ context.SBox[2][tmpBlock.Xr[1]]) +
				context.SBox[3][tmpBlock.Xr[0]])
			i += 2
			if i > BFRounds {
				break
			}
		}
		block[1] = block[1] ^ context.PBox[BFRounds+1]
	} else { // decrypting
		block[1] = block[1] ^ context.PBox[BFRounds+1]
		// {16 Rounds to go (8 double rounds to avoid swaps)}
		i := BFRounds
		for {
			//{first half round }
			block[0] = block[0] ^ context.PBox[i] ^ (((context.SBox[0][tmpBlock.Xr[3]] +
				context.SBox[1][tmpBlock.Xr[2]]) ^ context.SBox[2][tmpBlock.Xr[1]]) +
				context.SBox[3][tmpBlock.Xr[0]])
			//{second half round }
			block[1] = block[1] ^ context.PBox[i-1] ^ (((context.SBox[0][tmpBlock.Xl[3]] +
				context.SBox[1][tmpBlock.Xl[2]]) ^ context.SBox[2][tmpBlock.Xl[1]]) +
				context.SBox[3][tmpBlock.Xl[0]])
			i -= 2
			if i < 1 {
				break
			}
		}
		block[0] = block[0] ^ context.PBox[0]
	}
}

func encryptBFCBC(context *TBFContext, prev []byte, block *TBFBlock, encrypt bool) {
	var byteBlock []byte
	if encrypt {
		_ = binaryhelper.CopyFixed(block, &byteBlock)
		lockbox.XorMem(byteBlock, prev)
		_ = binaryhelper.CopyFixed(byteBlock, block)
		encryptBF(context, block, true)
	} else {
		encryptBF(context, block, false)
		_ = binaryhelper.CopyFixed(block, &byteBlock)
		lockbox.XorMem(byteBlock, prev)
		_ = binaryhelper.CopyFixed(byteBlock, block)
	}
}

func encryptBFByte(context *TBFContext, byteBlock *[]byte, encrypt bool) {
	var block TBFBlock
	_ = binaryhelper.CopyFixed(byteBlock, &block)
	encryptBF(context, &block, encrypt)
	_ = binaryhelper.CopyFixed(block, byteBlock)
}

func encryptBFCBCByte(context *TBFContext, prev []byte, byteBlock *[]byte, encrypt bool) {
	var block TBFBlock
	_ = binaryhelper.CopyFixed(byteBlock, &block)
	encryptBFCBC(context, prev, &block, encrypt)
	_ = binaryhelper.CopyFixed(block, byteBlock)
}

func BFEncryptStream(bb []byte, w io.Writer, key lockbox.TKey128, encrypt bool) error {
	var context TBFContext
	initEncryptBF(key, &context)
	// {get the number of blocks in the file}
	blockCount := len(bb) / TBFBlockByteSize
	// {when encrypting, make sure we have a block with at least one free}
	// {byte at the end. used to store the odd byte count value}
	if encrypt {
		blockCount++
	}
	inStream := bytes.NewBuffer(bb)
	block := make([]byte, TBFBlockByteSize)
	// {process all except the last block}
	for i := 0; i < blockCount-1; i++ {
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != TBFBlockByteSize {
			return lockbox.ErrInvalidFileFormat
		}
		encryptBFByte(&context, &block, encrypt)
		_, _ = w.Write(block)
	}
	// process the last block
	if encrypt { // encrypting
		// fill block with randomness, since unused (in the last block) remainder still contains data from the previous iteration
		_, _ = cryptorand.Read(block)
		// {set actual number of bytes to read}
		i := len(bb) % TBFBlockByteSize
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != i {
			return lockbox.ErrInvalidFileFormat
		}
		// {store number of bytes as last byte in last block}
		block[TBFBlockByteSize-1] = byte(i)
		// {encrypt and save full block}
		encryptBFByte(&context, &block, true)
		_, _ = w.Write(block)
	} else { // decrypting
		// {encrypted file is always a multiple of the block size}
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != TBFBlockByteSize {
			return lockbox.ErrInvalidFileFormat
		}
		encryptBFByte(&context, &block, false)
		// {get actual number of bytes encoded}
		i := block[TBFBlockByteSize-1]
		// {save valid portion of block}
		_, _ = w.Write(block[:i])
	}
	return nil
}

func BFEncryptStreamCBC(bb []byte, w io.Writer, key lockbox.TKey128, encrypt bool) error {
	var context TBFContext
	initEncryptBF(key, &context)
	// {get the number of blocks in the file}
	blockCount := len(bb) / TBFBlockByteSize
	inStream := bytes.NewBuffer(bb)
	block := make([]byte, TBFBlockByteSize)
	var IV, Work []byte
	if encrypt {
		// {set up an initialization vector (IV)}
		_, _ = cryptorand.Read(block)
		encryptBFByte(&context, &block, true)
		_, _ = w.Write(block)
		IV = block
	} else {
		// {read the frist block to prime the IV}
		if _, err := inStream.Read(block); err != nil {
			return err
		}
		blockCount--
		IV = block
	}
	// {when encrypting, make sure we have a block with at least one free}
	// {byte at the end. used to store the odd byte count value}
	if encrypt {
		blockCount++
	}
	// {process all except the last block}
	for i := 0; i < blockCount-1; i++ {
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != TBFBlockByteSize {
			return lockbox.ErrInvalidFileFormat
		}
		if encrypt {
			encryptBFCBCByte(&context, IV, &block, true)
			IV = block
		} else {
			Work = block
			encryptBFCBCByte(&context, IV, &block, false)
			IV = Work
		}
		_, _ = w.Write(block)
	}
	// process the last block
	if encrypt {
		_, _ = cryptorand.Read(block)
		// {set actual number of bytes to read}
		i := len(bb) % TBFBlockByteSize
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != i {
			return lockbox.ErrInvalidFileFormat
		}
		// {store number of bytes as last byte in last block}
		block[TBFBlockByteSize-1] = byte(i)
		// {encrypt and save full block}
		encryptBFCBCByte(&context, IV, &block, true)
		_, _ = w.Write(block)
	} else {
		// {encrypted file is always a multiple of the block size}
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != TBFBlockByteSize {
			return lockbox.ErrInvalidFileFormat
		}
		encryptBFCBCByte(&context, IV, &block, false)
		// {get actual number of bytes encoded}
		i := block[TBFBlockByteSize-1]
		// {save valid portion of block}
		_, _ = w.Write(block[:i])
	}
	return nil
}

func bfEncryptFilePrim(inFile, outFile string, key lockbox.TKey128, encrypt bool,
	encryptStream func([]byte, io.Writer, lockbox.TKey128, bool) error) error {
	bb, err := os.ReadFile(inFile)
	if err != nil {
		return err
	}
	f, err := os.Create(outFile)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := encryptStream(bb, f, key, encrypt); err != nil {
		return err
	}
	return nil
}

func BFEncryptFile(inFile, outFile string, key lockbox.TKey128, encrypt bool) error {
	return bfEncryptFilePrim(inFile, outFile, key, encrypt, BFEncryptStream)
}

func BFEncryptFileCBC(inFile, outFile string, key lockbox.TKey128, encrypt bool) error {
	return bfEncryptFilePrim(inFile, outFile, key, encrypt, BFEncryptStreamCBC)
}
