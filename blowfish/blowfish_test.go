package blowfish

import (
	"fmt"
	"testing"

	lockbox "gitlab.com/solsw/TurboPower/LockBox"
)

func Test_initEncryptBF(t *testing.T) {
	type args struct {
		key     lockbox.TKey128
		context *TBFContext
	}
	tests := []struct {
		name string
		args args
	}{
		{name: "1", args: args{context: &TBFContext{}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			initEncryptBF(tt.args.key, tt.args.context)
		})
	}
}

var key = lockbox.TKey128{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}

func ExampleBFEncryptFile_s1() {
	if err := BFEncryptFile("s.1", "s.2", key, true); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
	// Output: OK
}

func ExampleBFEncryptFile_s2() {
	if err := BFEncryptFile("s.2", "s.3", key, false); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
	// Output: OK
}

func ExampleBFEncryptFileCBC_s1() {
	if err := BFEncryptFileCBC("s.1", "s.4", key, true); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
	// Output: OK
}

func ExampleBFEncryptFileCBC_s2() {
	if err := BFEncryptFileCBC("s.4", "s.5", key, false); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
	// Output: OK
}
