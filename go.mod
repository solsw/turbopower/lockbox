module gitlab.com/solsw/TurboPower/LockBox

go 1.18

require (
	github.com/solsw/binaryhelper v0.1.3
	github.com/solsw/mathhelper v0.2.1
)

require golang.org/x/exp v0.0.0-20231226003508-02704c960a9b // indirect
