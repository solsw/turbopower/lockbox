package lockbox

import (
	"bytes"
	cryptorand "crypto/rand"
	"io"
	"os"

	"github.com/solsw/binaryhelper"
	"github.com/solsw/mathhelper"
)

// { LockBox Cipher }

// TLBCContext = packed record
//
//	Encrypt : Boolean;
//	Dummy   : array[0..2] of Byte; {filler}
//	Rounds  : LongInt;
//	case Byte of
//		0: (SubKeys64   : array [0..15] of TKey64);
//		1: (SubKeysInts : array [0..3, 0..7] of LongInt);
//
// end;
type TLBCContext struct {
	Encrypt   bool
	_         [3]byte // {filler}
	Rounds    int
	SubKeys64 [16]TKey64
}

func (c *TLBCContext) SubKeysInts() *[4][8]int32 {
	return Cast[[16]TKey64, [4][8]int32](&c.SubKeys64)
}

const TLBCBlockByteSize = 16

type (
	// TLBCBlock  = array[0..3] of LongInt;     { LockBox Cipher }
	TLBCBlock = [4]int32

	// TBCHalfBlock = array [0..1] of LongInt;
	TBCHalfBlock = [2]int32
)

// const
//
//	BCSalts: array [0..3] of DWord =
//	  ($55555555, $AAAAAAAA, $33333333, $CCCCCCCC);
var BCSalts = [4]uint32{0x55555555, 0xAAAAAAAA, 0x33333333, 0xCCCCCCCC}

func initEncryptLBC(key TKey128, context *TLBCContext, rounds int32, encrypt bool) {
	keyArray := Cast[TKey128, [4]int32](&key)
	context.Encrypt = encrypt
	context.Rounds = int(mathhelper.IntMax(4, mathhelper.IntMin(16, rounds)))

	// {fill subkeys by propagating seed}
	for i := 0; i < 4; i++ {
		// {interleave the key with the salt}
		AA := (*keyArray)[0]
		BB := *Cast[uint32, int32](&BCSalts[i])
		CC := (*keyArray)[1]
		DD := BB
		EE := (*keyArray)[2]
		FF := BB
		GG := (*keyArray)[3]
		HH := BB

		// {mix all the bits around for 8 rounds}
		// {achieves avalanche and eliminates funnels}
		for r := 0; r < 8; r++ {
			AA = AA ^ (BB << 11)
			DD = DD + AA
			BB = BB + CC
			BB = BB ^ (CC >> 2)
			EE = EE + BB
			CC = CC + DD
			CC = CC ^ (DD << 8)
			FF = FF + CC
			DD = DD + EE
			DD = DD ^ (EE >> 16)
			GG = GG + DD
			EE = EE + FF
			EE = EE ^ (FF << 10)
			HH = HH + EE
			FF = FF + GG
			FF = FF ^ (GG >> 4)
			AA = AA + FF
			GG = GG + HH
			GG = GG ^ (HH << 8)
			BB = BB + GG
			HH = HH + AA
			HH = HH ^ (AA >> 9)
			CC = CC + HH
			AA = AA + BB
		}

		// {assign value to subkey}
		ski := context.SubKeysInts()
		ski[i][0] = AA
		ski[i][1] = BB
		ski[i][2] = CC
		ski[i][3] = DD
		ski[i][4] = EE
		ski[i][5] = FF
		ski[i][6] = GG
		ski[i][7] = HH
	}
}

func encryptLBC(context *TLBCContext, block *TLBCBlock) {
	var blocks [2]TBCHalfBlock
	var work TBCHalfBlock
	_ = binaryhelper.CopyFixed(block, &blocks)
	right := blocks[0]
	left := blocks[1]
	for r := 0; r < context.Rounds; r++ {
		// {transform the right side}
		halfBlock := Cast[TKey64, TBCHalfBlock](&context.SubKeys64[r])
		AA := right[0]
		BB := halfBlock[0]
		CC := right[1]
		DD := halfBlock[1]

		// {mix it once...}
		AA = AA + DD
		DD = DD + AA
		AA = AA ^ (AA >> 7)
		BB = BB + AA
		AA = AA + BB
		BB = BB ^ (BB << 13)
		CC = CC + BB
		BB = BB + CC
		CC = CC ^ (CC >> 17)
		DD = DD + CC
		CC = CC + DD
		DD = DD ^ (DD << 9)
		AA = AA + DD
		DD = DD + AA
		AA = AA ^ (AA >> 3)
		BB = BB + AA
		AA = AA + BB
		BB = BB ^ (BB << 7)
		CC = CC + BB
		BB = BB + CC
		CC = CC ^ (DD >> 15)
		DD = DD + CC
		CC = CC + DD
		DD = DD ^ (DD << 11)

		// {swap sets...}
		AA, CC = CC, AA
		BB, DD = DD, BB

		// {mix it twice}
		AA = AA + DD
		DD = DD + AA
		AA = AA ^ (AA >> 7)
		BB = BB + AA
		AA = AA + BB
		BB = BB ^ (BB << 13)
		CC = CC + BB
		BB = BB + CC
		CC = CC ^ (CC >> 17)
		DD = DD + CC
		CC = CC + DD
		DD = DD ^ (DD << 9)
		AA = AA + DD
		DD = DD + AA
		AA = AA ^ (AA >> 3)
		BB = BB + AA
		AA = AA + BB
		BB = BB ^ (BB << 7)
		CC = CC + BB
		BB = BB + CC
		CC = CC ^ (DD >> 15)
		DD = DD + CC
		CC = CC + DD
		DD = DD ^ (DD << 11)

		work[0] = left[0] ^ AA ^ BB
		work[1] = left[1] ^ CC ^ DD

		left = right
		right = work
	}
	blocks[0] = left
	blocks[1] = right
	_ = binaryhelper.CopyFixed(blocks, block)
}

func encryptLBCByte(context *TLBCContext, byteBlock *[]byte) {
	var block TLBCBlock
	_ = binaryhelper.CopyFixed(byteBlock, &block)
	encryptLBC(context, &block)
	_ = binaryhelper.CopyFixed(block, byteBlock)
}

func LBCEncryptStream(bb []byte, w io.Writer, key TKey128, rounds int32, encrypt bool) error {
	var context TLBCContext
	initEncryptLBC(key, &context, rounds, encrypt)
	// {get the number of blocks in the file}
	blockCount := len(bb) / TLBCBlockByteSize
	// {when encrypting, make sure we have a block with at least one free}
	// {byte at the end. used to store the odd byte count value}
	if encrypt {
		blockCount++
	}
	inStream := bytes.NewBuffer(bb)
	block := make([]byte, TLBCBlockByteSize)
	// {process all except the last block}
	for i := 0; i < blockCount-1; i++ {
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != TLBCBlockByteSize {
			return ErrInvalidFileFormat
		}
		encryptLBCByte(&context, &block)
		_, _ = w.Write(block)
	}
	if encrypt { // encrypting
		_, _ = cryptorand.Read(block)
		// {set actual number of bytes to read}
		i := len(bb) % TLBCBlockByteSize
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != i {
			return ErrInvalidFileFormat
		}
		// {store number of bytes as last byte in last block}
		block[TLBCBlockByteSize-1] = byte(i)
		// {encrypt and save full block}
		encryptLBCByte(&context, &block)
		_, _ = w.Write(block)
	} else { // decrypting
		// {encrypted file is always a multiple of the block size}
		n, err := inStream.Read(block)
		if err != nil {
			return err
		}
		if n != TLBCBlockByteSize {
			return ErrInvalidFileFormat
		}
		encryptLBCByte(&context, &block)
		// {get actual number of bytes encoded}
		i := block[TLBCBlockByteSize-1]
		// {save valid portion of block}
		_, _ = w.Write(block[:i])
	}
	return nil
}

func LBCEncryptFile(inFile, outFile string, key TKey128, rounds int32, encrypt bool) error {
	bb, err := os.ReadFile(inFile)
	if err != nil {
		return err
	}
	f, err := os.Create(outFile)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := LBCEncryptStream(bb, f, key, rounds, encrypt); err != nil {
		return err
	}
	return nil
}
