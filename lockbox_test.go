package lockbox

import (
	"fmt"
	"testing"
)

var key = TKey128{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}

func Test_initEncryptLBC(t *testing.T) {
	type args struct {
		key     TKey128
		context *TLBCContext
		rounds  int32
		encrypt bool
	}
	tests := []struct {
		name string
		args args
	}{
		{name: "1", args: args{key: key, context: &TLBCContext{}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			initEncryptLBC(tt.args.key, tt.args.context, tt.args.rounds, tt.args.encrypt)
		})
	}
}

func TestLBCEncryptFile(t *testing.T) {
	type args struct {
		inFile  string
		outFile string
		key     TKey128
		rounds  int32
		encrypt bool
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "0",
			args:    args{inFile: "qazxswed"},
			wantErr: true,
		},
		{name: "1",
			args: args{
				inFile:  "s.1",
				outFile: "s.2",
				key:     key,
				rounds:  4,
				encrypt: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := LBCEncryptFile(tt.args.inFile, tt.args.outFile, tt.args.key, tt.args.rounds, tt.args.encrypt); (err != nil) != tt.wantErr {
				t.Errorf("LBCEncryptFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func ExampleLBCEncryptFile_s1() {
	if err := LBCEncryptFile("s.1", "s.2", key, 1, true); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
	// Output: OK
}

func ExampleLBCEncryptFile_s2() {
	if err := LBCEncryptFile("s.2", "s.3", key, 1, false); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("OK")
	}
	// Output: OK
}
